#if !defined(GS_H)
#define      GS_H

#include <stddef.h>

/** Gauss-Seidel iteration on a 3D unit cube discretization of the Poisson
 *  equation $-\Delta u = f$ with homogeneous Dirichlet boundary conditions,
 *  using a 21-point stencil:
 *
 *        8
 *       -- : stencil center
 *       3n
 *
 *       -1
 *       -- : stencil edge neighbors
 *       6n
 *
 *       -1
 *      --- : stencil vertex neighbors
 *      12n

  \param[in]     n         The number of cells per dimension (one more than the
                           number of degrees of freedom per dimension)
  \param[in]     n_iter    The number of Gauss-Seidel iterations to perform.
  \param[in]     f         [(n - 1) x (n - 1) x (n - 1) vector] The right hand side.
  \param[in/out] u         [(n - 1) x (n - 1) x (n - 1) vector] The solution
                           variables.  Degrees of freedom are ordered
                           lexicographically. The function should perform n_iter
                           iterations of the update

                               u := u + (L + D)^{-1} (b - K*u),

                           where K is the matrix representation of applying the
                           stencil, and K = L + D + L^T is a decomposition of
                           K with into diagonal (D), lower triangular (L) and upper
                           triangular (L^T) components.  Note that this
                           decomposition may be with respect to a different
                           ordering than lexicographic ordering: see
                           gauss_seidel_ordering().
 */

int gauss_seidel_iteration(size_t n, int n_iter, const double *f, double *u);

/** Report the ordering used by gauss_seidel_iteration().

  \param[in]  n       The number of cells per dimension (same meaning as in
                      gauss_seidel_iteration().
  \param[out] order   [(n - 1) x (n - 1) x (n - 1)] array of the ordering of
                      indices used in gauss_seidel_iteration().
 */
int gauss_seidel_ordering(size_t n, size_t *order);


#endif
